<div class="widget-container">
    <div class="picture1">
        <img src="<?php echo $settings['picture1_image']['url'] ?>" alt="<?php echo $settings['picture1_image']['alt'] ?>" />
    </div>
    <div class="text1">
        <?php echo $settings['picture1_text'] ?>
    </div>
    <div class="picture2">
        <img src="<?php echo $settings['picture2_image']['url'] ?>" alt="<?php echo $settings['picture2_image']['alt'] ?>" />

    </div>
    <div class="text2">
        <?php echo $settings['picture2_text'] ?>

    </div>
    <div class="picture3">
        <img src="<?php echo $settings['picture3_image']['url'] ?>" alt="<?php echo $settings['picture3_image']['alt'] ?>" />

    </div>
    <div class="text3">
        <?php echo $settings['picture3_text'] ?>

    </div>
</div>