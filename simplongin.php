<?php

/**
 * Plugin Name: Simplongin
 */

add_action('admin_menu', 'plugin_menu');

function plugin_menu()
{
    add_menu_page(
        'Simplon menu',
        'Simplon Menu',
        'manage_options',
        'simplon_menu',
        'plugin_page'
    );

    add_action('admin_init', 'plugin_settings');
}

function plugin_settings()
{
    register_setting('simplongin_settings', 'first_setting');
    register_setting('simplongin_settings', 'admin_color', ['default' => '#ffb8b8']);
}

function plugin_page()
{
    require 'admin-menu.php';
}

add_action('admin_head', 'plugin_style');
add_action('wp_head', 'plugin_style');

function plugin_style()
{
    require 'admin-style.php';
}


add_action('widgets_init', 'plugin_widget');

function plugin_widget() {
    require 'first-widget/first-widget.php';
    register_widget('FirstWidget');
    require 'fabrique-widget/fabrique-widget.php';
    register_widget('FabriqueWidget');
}


require 'elementor-extension.php';