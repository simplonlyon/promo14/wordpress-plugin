<?php


class FirstWidget extends WP_Widget {

    public function __construct() {
        parent::__construct('first_widget', 'First Example Widget');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        require 'widget-template.php';
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        $content = isset($instance['content']) ? $instance['content']:'Default content';
        require 'widget-form.php';
    }

}


