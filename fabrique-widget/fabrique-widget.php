<?php


class FabriqueWidget extends WP_Widget {

    public function __construct() {
        parent::__construct('fabrique_widget', 'A widget displaying fabriques');
    }

    public function widget($args, $instance)
    {
        $query = new WP_Query([
            'post_type' => 'fabrique',
            'posts_per_page' => $instance['limit']
        ]);

        echo $args['before_widget'];
        require 'widget-template.php';
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        $limit = isset($instance['limit']) ? $instance['limit']:0;
        require 'widget-form.php';
    }

}


