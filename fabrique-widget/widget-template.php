
<ul>
<?php
while ($query->have_posts()) : $query->the_post();

?>

    <li>
        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
    </li>

<?php
endwhile;
?>

</ul>