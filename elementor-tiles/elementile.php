<?php

class Elementile extends \Elementor\Widget_Base
{

    public function __construct($data = array(), $args = null)
    {
        parent::__construct($data, $args);

        wp_register_style('elementile-style', plugins_url('/elementile.css', __FILE__), array(), '1.0.0');
    }

    /**
     * Enqueue styles.
     */
    public function get_style_depends()
    {
        return array('elementile-style');
    }

    public function get_name()
    {
        return 'elementile';
    }

    public function get_title()
    {
        return 'Elementile';
    }

    public function get_icon()
    {
        return 'fa fa-square';
    }

    public function get_categories()
    {
        return ['general'];
    }


    protected function _register_controls()
    {
        $this->start_controls_section('picture1_section', [
            'label' => 'Picture 1 Settings',
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT
        ]);
        $this->add_control('picture1_image', [
            'label' => 'Picture 1 Image',
            'type' => \Elementor\Controls_Manager::MEDIA
        ]);

        $this->add_control('picture1_text', [
            'label' => 'Picture 1 Text',
            'type' => \Elementor\Controls_Manager::TEXTAREA
        ]);
        $this->end_controls_section();


        $this->start_controls_section('picture2_section', [
            'label' => 'Picture 2 Settings',
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT
        ]);
        $this->add_control('picture2_image', [
            'label' => 'Picture 2 Image',
            'type' => \Elementor\Controls_Manager::MEDIA
        ]);

        $this->add_control('picture2_text', [
            'label' => 'Picture 2 Text',
            'type' => \Elementor\Controls_Manager::TEXTAREA
        ]);
        $this->end_controls_section();


        $this->start_controls_section('picture3_section', [
            'label' => 'Picture 3 Settings',
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT
        ]);
        $this->add_control('picture3_image', [
            'label' => 'Picture 3 Image',
            'type' => \Elementor\Controls_Manager::MEDIA
        ]);

        $this->add_control('picture3_text', [
            'label' => 'Picture 3 Text',
            'type' => \Elementor\Controls_Manager::TEXTAREA
        ]);
        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        require 'template.php';
    }
}
